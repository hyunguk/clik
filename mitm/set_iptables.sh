#!/bin/bash
ip_addr=$(ifconfig ens33 | grep 'inet addr' | cut -d: -f2 | awk '{print $1}')
sudo echo "1" > /proc/sys/net/ipv4/ip_forward
sudo iptables -t nat -A PREROUTING -p tcp --dport 502 -j DNAT --to-destination $ip_addr:502


