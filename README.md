CLIK on PLCs!
================

These source code and dataset are the research artifacts described in our paper. 


Paper
------------
Sushma Kalle, Nehal Ameen, Hyunguk Yoo, and Irfan Ahmed, CLIK on PLCs! Attacking Control Logic with Decompilation and Virtual PLC, BAR 2019