# Extract all files from network traffic

from scapy.all import *
import sys
import os
import argparse

# M221 message (modbus payload after the function code) offset in TCP payload
M221_OFFSET = 8 
MODBUS_PORT = 502

class extractor_up():
    def extract(self, pcapFile):
        all_pkts = rdpcap(pcapFile)
        file_path = str(os.path.abspath(pcapFile))
        save_dir = file_path.split(".")[0] + "/upload/"

        pkts = (pkt for pkt in all_pkts if
            TCP in pkt and (pkt[TCP].sport == MODBUS_PORT or pkt[TCP].dport == MODBUS_PORT) and len(pkt[TCP].payload) > M221_OFFSET)


        next_addr = 0    # initialization

        readFlag = False
        for pkt in pkts:
            msg = bytes(pkt[TCP].payload)[M221_OFFSET:]
            # read request
            if pkt[TCP].dport == MODBUS_PORT and msg[1] == '\x28':
                readFlag = True

                addr = msg[2:4]
                fileType = msg[4:6]
                size = msg[6:8]

                if next_addr != struct.unpack("<H", addr)[0]:
                    save_file_path = save_dir + binascii.hexlify(msg[2:6])
                    
                    if not os.path.exists(os.path.dirname(save_file_path)):
                        try:
                            os.makedirs(os.path.dirname(save_file_path))
                        except:
                            print "exception occur in making dir: " + os.path.dirname(os.path.dirname(save_file_path))

                    print "Create file: " + save_file_path
                    out_file = open(save_file_path, "w")
                    
                next_addr = struct.unpack("<H", addr)[0] + struct.unpack("<H", size)[0]
            elif pkt[TCP].sport == MODBUS_PORT and readFlag == True:
                out_file.write(msg[4:])
                # prevent duplicated write if there is TCP retransmission
                readFlag = False
            else:
                readFlag = False
                

def main():
    parser = argparse.ArgumentParser(description="Extract files related to control logic from a upload pcap file")
    parser.add_argument("pcapFile", help="target pcap file from which files are extracted")
    args = parser.parse_args()

    extractor_up().extract(args.pcapFile)

if __name__ == '__main__':
    main()
