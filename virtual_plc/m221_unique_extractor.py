from scapy.all import *
from scapy.contrib.modbus import *

import sys
import argparse

# Extract unique m221 request/response messages pairs
class M221_unique():
    def __init__(self, exclude, refine):
        self.unique_reqres = []
        self.exclude = exclude
        self.refine = refine

    def extract_unique(self, pcapFile):
        pkts = rdpcap(pcapFile)

        # To check consecutive request or response
        req_flag = 0
        res_flag = 0

        frame_num = 0

        req_pkt = 0
        res_pkt = 0

        for pkt in pkts:
            frame_num += 1
            if pkt.haslayer(ModbusADURequest):
                if req_pkt == pkt[TCP]:
                    #print "#{}: TCP Retransmission".format(frame_num)
                    continue

                req_pkt = pkt[TCP]

                res_flag = 0
                if req_flag == 0:
                    req_flag = 1
                else:
                    print "#{}: consecutive requests are observed without response".format(frame_num)


            elif pkt.haslayer(ModbusADUResponse):
                if res_pkt == pkt[TCP]:
                    #print "#{}: TCP Retransmission".format(frame_num)
                    continue

                if req_flag == 0:
                    #print "No previous request"
                    continue

                res_pkt = pkt[TCP]

                m221_req = bytes(req_pkt[ModbusADURequest].payload)[1:] # except modbus function code. only m221 message
                m221_res = bytes(res_pkt[ModbusADUResponse].payload)[1:]

                req_flag = 0
                if res_flag == 0:
                    res_flag = 1
                else:
                    print "#{}: consecutive response are observed without request".format(frame_num)
                
                # Exclude m221 read (0x28) or write (0x29) request and response
                if self.exclude and (m221_req[1] == '\x28' or m221_req[1] == '\x29'):
                    continue
                if self.refine and m221_req in [a for a,b in self.unique_reqres]:
                    continue
                if (m221_req, m221_res) not in self.unique_reqres:
                    self.unique_reqres.append((m221_req, m221_res))

    def get_unique(self, pcapFile):
        self.extract_unique(pcapFile)
        return self.unique_reqres

    def print_unique(self):
        for (m221_req, m221_res) in self.unique_reqres:
            # if not read
            #if bytes(m221_req)[2] != '\x28':        
                t = iter(binascii.hexlify(m221_req))
                print "--> " + ":".join(a+b for a,b in zip(t,t))
            
                t = iter(binascii.hexlify(m221_res))
                print "<-- " + ":".join(a+b for a,b in zip(t,t))

def main():
    parser = argparse.ArgumentParser(description="Extract unique request-response M221 message pairs")
    parser.add_argument("-e", "--exclude-read-write", help="exclude read/write request-response pairs", action="store_true")
    parser.add_argument("-r", "--refine", help="remain only one among pairs where their request messages are the same", action="store_true")
    parser.add_argument("pcapFile", help="target pcap file from which unique pairs are extracted")
    args = parser.parse_args()

    m221_unique = M221_unique(args.exclude_read_write, args.refine)
    m221_unique.extract_unique(args.pcapFile)
    m221_unique.print_unique()

if __name__ == '__main__':
    main()
